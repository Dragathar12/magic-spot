const expect = require('chai').expect

describe('Arithmetic operations', function() {

  const number1 = 5
  const number2 = 5

  it('Should be equal to 5', function() {
    const sum = number1 + number2
    expect(sum).to.equal(10)
  })

  it('Should be equal to 25', function() {
    const product = number1 * number2
    expect(product).to.equal(25)
  })

  it('Should be equal to 1', function() {
    const quotient = number1 / number2
    expect(quotient).to.equal(1)
  })

  it('Should be equal to 0', function() {
    const difference = number1 - number2
    expect(difference).to.equal(0)
  })

})
